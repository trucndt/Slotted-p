/*
 * DataDissemination.h
 *
 *  Created on: May 28, 2017
 *      Author: trucndt
 */

#ifndef DATADISSEMINATION_H_
#define DATADISSEMINATION_H_

#include "BaseWaveApplLayer.h"

class DataDissemination
{
public:
    virtual void processReceivedMessage(cMessage *msg) {};
    virtual void sendWarningMsg(const std::string &msg) {};
};

class Neighbor
{
public:
    Neighbor(const Coord& aPos, double aSpeed, double aDir, const simtime_t& aTime) :
        position(aPos), speed(aSpeed), direction(aDir), timeStamp(aTime) {}

    Coord position;
    double speed;
    double direction;
    simtime_t timeStamp;
};

#endif /* DATADISSEMINATION_H_ */
