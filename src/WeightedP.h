/*
 * WeightedPPersistence.h
 *
 *  Created on: Apr 8, 2017
 *      Author: trucndt
 */

#ifndef WEIGHTEDP_H_
#define WEIGHTEDP_H_

#include <stdint.h>
#include <vector>
#include <algorithm>
#include "DataDissemination.h"
#include "NodeMain.h"

class NodeMain;

class WeightedP: public DataDissemination
{
public:
    WeightedP(NodeMain* aNodeMain);

    enum stateEnum
    {
        IDLE,
        WAIT,
        BACKOFF,
        SEND
    };

    void processReceivedMessage(cMessage *msg);
    void sendWarningMsg(const std::string &msg);

private:
    static const int BUFFER_SIZE = 5;
    static const int MAX_FORWARD_DISTANCE = 2000;
    static const int WAIT_TIME_IN_MS = 6;

    uint8_t mState;
    NodeMain* mNode;
    cMessage* mTimeoutEvt;

    std::vector<long> mRcvMessageId;
    uint8_t mBufferIdx;

    WaveBroadcastMessage* mCurMsg;
    double mMinRelativeDistance;

    double calcProbability();
    void addToBuffer(long value);
};

#endif /* WEIGHTEDP_H_ */
