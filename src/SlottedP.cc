/*
 * Slotted1.cpp
 *
 *  Created on: Apr 28, 2017
 *      Author: trucndt
 */

#include <SlottedP.h>

using namespace std;

SlottedP::SlottedP(NodeMain* aNodeMain, double aProbability)
{
    mNode = aNodeMain;
    mState = IDLE;
    mTimeoutEvt = new cMessage("Timeout");
    mRcvMessageId.resize(BUFFER_SIZE);
    mBufferIdx = 0;
    mPredefinedProbability = aProbability;
}

void SlottedP::processReceivedMessage(cMessage* msg)
{
    switch (mState)
    {
        case IDLE:
        {
            EV << "IDLE: ";
            WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
            if (wbm == NULL) break;

            if (find(mRcvMessageId.begin(), mRcvMessageId.end(), wbm->getTreeId()) == mRcvMessageId.end())
            {
                // New message
                // Discard if not in ROI
                mNode->handleAccidentMsg();
                if (mNode->traci->getDistance(mNode->curPosition, wbm->getSenderPos(), false) > MAX_FORWARD_DISTANCE)
                {
//                    break;
                }

                addToBuffer(wbm->getTreeId());

                double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);
                mMinRelativeDistance = distance;
                EV << "Original distance " << distance << endl;

                mCurMsg = wbm->dup();
                mCurMsg->setForwardPos(mNode->curPosition);
                mCurMsg->setForwardAddress(mNode->myId);

                EV << "Change to state WAIT after  "<< WAIT_TIME_IN_MS << "ms" << endl;
                mNode->scheduleAt(simTime() + WAIT_TIME_IN_MS * 0.001, mTimeoutEvt);
                mState = WAIT;
            }
            else
            {
                // Duplicate
            }
            break;
        }
        case WAIT:
        {
            EV << "WAIT: ";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
            {
                int slotNum = calcSlotNumber();

                EV << "Slot number = " << slotNum << endl;

                double waitTime = slotNum * mNode->mPropagationDelay;
                EV << "Change to state WAIT_SLOT after " << waitTime << " second\n";
                mNode->scheduleAt(simTime() + waitTime, mTimeoutEvt);
                mState = WAIT_SLOT;
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
            {
                WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);

                double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);

                if (mMinRelativeDistance > distance)
                    mMinRelativeDistance = distance;

                EV << "Re-calculate the min distance: " << mMinRelativeDistance << endl ;
            }
        }
        break;
        case WAIT_SLOT:
        {
            EV << "WAIT_SLOT: ";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
            {
                if (mNode->dblrand() <= mPredefinedProbability)
                {
                    EV << "Change to state SEND\n";
                    mState = SEND;
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                }
                else
                {
                    mState = BACKOFF;
                    double waitTime = (NUM_OF_SLOTS - 1) * mNode->mPropagationDelay
                            + mNode->mPropagationDelay * mNode->dblrand();

                    EV << "Change to BACKOFF after " << waitTime << " second\n";
                    mNode->scheduleAt(simTime() + waitTime, mTimeoutEvt);
                }
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
            {
                mNode->cancelEvent(mTimeoutEvt);

                EV << "Going back to IDLE after duplication\n";
                mState = IDLE;
            }
            break;
        }
        case BACKOFF:
        {
            EV << "BACKOFF:";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
            {
                EV << "Change to state SEND\n";
                mState = SEND;
                mNode->scheduleAt(simTime(), mTimeoutEvt);
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
            {
                mNode->cancelEvent(mTimeoutEvt);

                EV << "Going back to IDLE after duplication\n";
                mState = IDLE;
            }
        }
        break;

        case SEND:
            EV << "SEND: ";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                mNode->sendWBM(mCurMsg);
                mState = IDLE;
                break;
            }
    }
}

void SlottedP::sendWarningMsg(const string& msg)
{
    WaveBroadcastMessage *wbm = mNode->prepareWBM("broadcast", mNode->dataLengthBits, type_CCH, mNode->dataPriority, -1, 2);
    wbm->setWsmData(msg.c_str());
    addToBuffer(wbm->getTreeId());
    mCurMsg = wbm->dup();
    mNode->sendWBM(wbm);
}

int SlottedP::calcSlotNumber()
{
    int slotNum = (double)NUM_OF_SLOTS *
            (1 - min(mMinRelativeDistance, (double)NodeMain::TRANSMISSION_RANGE)/(double)NodeMain::TRANSMISSION_RANGE);

    return slotNum;
}

void SlottedP::addToBuffer(long value)
{
    mRcvMessageId[mBufferIdx] = value;
    mBufferIdx = (mBufferIdx + 1) % BUFFER_SIZE;
}
