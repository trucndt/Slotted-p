//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <NodeMain.h>

using Veins::TraCIMobilityAccess;
using Veins::AnnotationManagerAccess;
using namespace std;

Define_Module(NodeMain);

void NodeMain::initialize(int stage)
{
    BaseWaveApplLayer::initialize(stage);

    if (stage == 0)
    {
        mobility = TraCIMobilityAccess().get(getParentModule());
        traci = mobility->getCommandInterface();
        traciVehicle = mobility->getVehicleCommandInterface();

        mPropagationDelay = par("maxOffset").doubleValue() + 120 * pow(10,-6);

        DBG << "Scheme: " << par("scheme").stringValue() << endl;

        if (strcmp(par("scheme").stringValue(), "SlottedP") == 0)
        {
            mWarningDissemination = new SlottedP(this, 0.5);
        }
        else if (strcmp(par("scheme").stringValue(), "Slotted1") == 0)
        {
            mWarningDissemination = new SlottedP(this, 1);
        }
        else if (strcmp(par("scheme").stringValue(), "WeightedP") == 0)
        {
            mWarningDissemination = new WeightedP(this);
        }
        else if (strcmp(par("scheme").stringValue(), "Flooding") == 0)
        {
            mWarningDissemination = new Flooding(this);
        }
        else if (strcmp(par("scheme").stringValue(), "DvCast") == 0)
        {
            mWarningDissemination = new DvCast(this);
        }
        else if (strcmp(par("scheme").stringValue(), "NSF") == 0)
        {
            mWarningDissemination = new NSF(this);
        }
        else if (strcmp(par("scheme").stringValue(), "BIM") == 0)
        {
            mWarningDissemination = new BIM(this);
        }

        mLastDrove = simTime();
        mSent = false;
        mNumOfCars = 0;
        mReceivedTime = 0;
        mNumOfRcvMsg = 0;
        mNumOfRcvBeacon = 0;
        mSentWBM = 0;
        WATCH(individualOffset);
    }
}

void NodeMain::receiveSignal(cComponent* source, simsignal_t signalID,
        cObject* obj, cObject* details)
{
    Enter_Method_Silent();
    if (signalID == mobilityStateChangedSignal)
    {
        handlePositionUpdate(obj);
    }
}

void NodeMain::onWBM(WaveBroadcastMessage* wbm)
{
    EV << "Received broadcast " << wbm->getWsmData() << " from " << wbm->getForwardAddress()
            << " position " << wbm->getForwardPos() << endl;
    EV << "Current position " << curPosition << "; Distance " << traci->getDistance(wbm->getForwardPos(), curPosition, false) << endl;

    wbm->setHopCount(wbm->getHopCount() + 1);
    mNumOfRcvMsg++;

    if (strcmp(wbm->getWsmData(), "Accident") == 0)
        mWarningDissemination->processReceivedMessage(wbm);
}

void NodeMain::onBeacon(WaveShortMessage* wsm)
{
    EV << "Received beacon " << wsm->getDisplayString() << " from " << wsm->getSenderAddress()
            << " position " << wsm->getSenderPos() << endl;
    EV << "Current position " << curPosition << "; Distance " << traci->getDistance(wsm->getSenderPos(), curPosition, false) << endl;
}

void NodeMain::onWBB(WaveBroadcastBeacon* wbb)
{
    EV << "Received beacon " << wbb->getDisplayString() << " from " << wbb->getSenderAddress()
            << " position " << wbb->getSenderPos() << endl;
    EV << "Current position " << curPosition << "; Distance " << traci->getDistance(wbb->getSenderPos(), curPosition, false) << endl;

    mNumOfRcvBeacon++;

    mWarningDissemination->processReceivedMessage(wbb);
}

void NodeMain::handleSelfMsg(cMessage* msg)
{
    switch (msg->getKind())
    {
        case SEND_BEACON_EVT:
        {
            WaveBroadcastBeacon *beacon = prepareWBB("beacon", beaconLengthBits, type_CCH, beaconPriority, 0, -1);

            if (BIM* bim = dynamic_cast<BIM*>(mWarningDissemination))
            {
                scheduleAt(simTime() + bim->calcBeaconInterval(), sendBeaconEvt);
                if (bim->mCurMsg != NULL)
                    beacon->setHasMessage(1);
                bim->processReceivedMessage(beacon);
            }
            else
                scheduleAt(simTime() + par("beaconInterval").doubleValue(), sendBeaconEvt);

            sendWSM(beacon);

            break;
        }
        default:
        {
            if (msg)
            {
                DBG << "APP: Error: Got Self Message of unknown kind! Name: " << msg->getName() << endl;
                mWarningDissemination->processReceivedMessage(msg);
            }
            break;
        }
    }
}

void NodeMain::handleAccidentMsg()
{
//    traciVehicle->setSpeed(0);
    mReceivedTime = simTime();

    findHost()->getDisplayString().updateWith("r=16,green");
}

void NodeMain::handlePositionUpdate(cObject* obj)
{
    BaseWaveApplLayer::handlePositionUpdate(obj);
    curSpeed = mobility->getSpeed();
    curDirection = mobility->getAngleRad();

//    /* GPS drift */
//    double xDrift = ((intrand(2) == 0) ? 1 : -1) * dblrand() * 50;
//    double yDrift = ((intrand(2) == 0) ? 1 : -1) * dblrand() * 50;
//    Coord drift(xDrift, yDrift);
//    curPosition += drift;
//    DBG << "GPS drift:" << drift << endl;

    if (myId != 0) return;
    // if stopped for two seconds
    if (mobility->getSpeed() == 0)
    {
        if (simTime() - mLastDrove >= 2)
        {
            if (!mSent)
            {
                findHost()->getDisplayString().updateWith("r=16,red");
                mWarningDissemination->sendWarningMsg("Accident");
                mReceivedTime = simTime();
                mSent = true;
                Veins::TraCIScenarioManager* manager = Veins::TraCIScenarioManagerAccess().get();
                mNumOfCars = manager->getManagedHosts().size();
                EV << "Current number of car: " << mNumOfCars << endl;
            }
        }
    }
    else
    {
        mLastDrove = simTime();
    }
}

void NodeMain::sendWBM(WaveBroadcastMessage* wbm)
{
    mSentWBM++;
    sendDelayedDown(wbm, individualOffset);
}

WaveBroadcastMessage* NodeMain::prepareWBM(const std::string& name, int dataLengthBits,
        t_channel channel, int priority, int rcvId, int serial)
{
    WaveBroadcastMessage* wbm = new WaveBroadcastMessage(name.c_str());
    wbm->setBitLength(headerLength);
    wbm->addBitLength(dataLengthBits);

    switch (channel)
    {
        case type_SCH: wbm->setChannelNumber(Channels::SCH1); break; //will be rewritten at Mac1609_4 to actual Service Channel. This is just so no controlInfo is needed
        case type_CCH: wbm->setChannelNumber(Channels::CCH); break;
    }

    wbm->setPsid(0);
    wbm->setPriority(priority);
    wbm->setWsmVersion(1);
    wbm->setTimestamp(simTime());
    wbm->setSenderAddress(myId);
    wbm->setRecipientAddress(rcvId);
    wbm->setSenderPos(curPosition);
    wbm->setForwardPos(curPosition);
    wbm->setSerial(serial);
    wbm->setHopCount(0);

    DBG << "Creating a new WaveBroadcastMessage\n";

    return wbm;
}

WaveBroadcastBeacon* NodeMain::prepareWBB(const std::string& name, int dataLengthBits,
        t_channel channel, int priority, int rcvId, int serial)
{
    WaveBroadcastBeacon* wbb = new WaveBroadcastBeacon(name.c_str());
    wbb->setBitLength(headerLength);
    wbb->addBitLength(dataLengthBits);

    switch (channel)
    {
        case type_SCH: wbb->setChannelNumber(Channels::SCH1); break; //will be rewritten at Mac1609_4 to actual Service Channel. This is just so no controlInfo is needed
        case type_CCH: wbb->setChannelNumber(Channels::CCH); break;
    }

    wbb->setPsid(0);
    wbb->setPriority(priority);
    wbb->setWsmVersion(1);
    wbb->setTimestamp(simTime());
    wbb->setSenderAddress(myId);
    wbb->setSenderPos(curPosition);
    wbb->setRecipientAddress(rcvId);
    wbb->setSpeed(mobility->getSpeed());
    wbb->setDirection(mobility->getAngleRad());
    wbb->setSerial(serial);
    wbb->setHasMessage(0);

    DBG << "Creating a new WaveBroadcastBeacon\n";

    return wbb;
}

void NodeMain::handleLowerMsg(cMessage* msg)
{
    if (strcmp(msg->getName(), "beacon") == 0)
    {
        onWBB(check_and_cast<WaveBroadcastBeacon*>(msg));
    }
    else if (strcmp(msg->getName(), "broadcast") == 0)
    {
        onWBM(check_and_cast<WaveBroadcastMessage*>(msg));
    }
    else
    {
        DBG << "unknown message (" << msg->getName() << ")  received\n";
    }
    delete(msg);
}

void NodeMain::finish()
{
    BaseWaveApplLayer::finish();

    if (myId == 0) recordScalar("#Cars", mNumOfCars);
    recordScalar("ReceivedTime", mReceivedTime);
    recordScalar("#RcvMsg", mNumOfRcvMsg);
    recordScalar("#RcvBeacon", mNumOfRcvBeacon);
    recordScalar("#Sent", mSentWBM);
}
