//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef NODEMAIN_H_
#define NODEMAIN_H_

#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCIMobility.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "WaveBroadcastMessage_m.h"
#include "WaveBroadcastBeacon_m.h"
#include "Flooding.h"
#include "WeightedP.h"
#include "SlottedP.h"
#include "DvCast.h"
#include "DataDissemination.h"
#include "NSF.h"
#include "BIM.h"

using Veins::TraCIMobility;
using Veins::TraCICommandInterface;
using Veins::AnnotationManager;

class Flooding;
class WeightedP;
class SlottedP;
class DvCast;
class NSF;
class BIM;

class NodeMain: public BaseWaveApplLayer
{
public:
    virtual void initialize(int stage);
    virtual void receiveSignal(cComponent* source, simsignal_t signalID, cObject* obj, cObject* details);
    void handleAccidentMsg();

protected:
    virtual void onData(WaveShortMessage* wsm){};
    virtual void onBeacon(WaveShortMessage* wsm);
    virtual void handleSelfMsg(cMessage* msg);
    virtual void handlePositionUpdate(cObject* obj);
    virtual void handleLowerMsg(cMessage* msg);
    virtual void finish();

    virtual void onWBM(WaveBroadcastMessage* wbm);
    virtual void onWBB(WaveBroadcastBeacon* wbb);
    virtual void sendWBM(WaveBroadcastMessage* wbm);
    virtual WaveBroadcastMessage* prepareWBM(const std::string& name, int dataLengthBits, t_channel channel, int priority, int rcvId, int serial=0);
    virtual WaveBroadcastBeacon* prepareWBB(const std::string& name, int dataLengthBits, t_channel channel, int priority, int rcvId, int serial=0);

    double curSpeed;
    double curDirection;

    TraCIMobility* mobility;
    TraCICommandInterface* traci;
    TraCICommandInterface::Vehicle* traciVehicle;
    cMessage* mAccidentEvt;
    DataDissemination* mWarningDissemination;
    simtime_t mLastDrove;
    bool mSent;

    int mNumOfCars;
    simtime_t mReceivedTime;
    int mNumOfRcvMsg;
    int mNumOfRcvBeacon;
    int mSentWBM;

    double mPropagationDelay;
    static const simsignalwrap_t parkingStateChangedSignal;
    static const int TRANSMISSION_RANGE = 366;

    friend class Flooding;
    friend class WeightedP;
    friend class SlottedP;
    friend class DvCast;
    friend class NSF;
    friend class BIM;
};

#endif /* NODEMAIN_H_ */
