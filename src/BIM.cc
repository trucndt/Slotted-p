/*
 * BIM.cc
 *
 *  Created on: Jun 28, 2017
 *      Author: trucndt
 */

#include <BIM.h>

using namespace std;

BIM::BIM(NodeMain* aNodeMain)
{
    mNode = aNodeMain;
    mRcvMessageId.resize(BUFFER_SIZE, -1);
    mBufferIdx = 0;
    mHeartbeatEvt = new cMessage("Heartbeat");
    mTimeoutEvt = new cMessage("Timeout");
    mNode->scheduleAt(simTime() + (HEARTBEAT_THRESHOLD/10.0), mHeartbeatEvt);
    mPacketTimeoutEvt = new cMessage("PktTimeout");
    mState = IDLE;
    mCurMsg = NULL;

    Neighbor* newNeighbor = new Neighbor(mNode->mobility->getCurrentPosition(), mNode->mobility->getSpeed(), mNode->mobility->getAngleRad(), simTime());
    mNeighborList[mNode->myId] = newNeighbor;
}

BIM::~BIM()
{

}

void BIM::processReceivedMessage(cMessage* msg)
{
    if (WaveBroadcastBeacon* wb = dynamic_cast<WaveBroadcastBeacon*>(msg))
    {
        // it is a beacon
        DBG << "Direction: " << wb->getDirection() << endl;
        DBG << "Speed: " << wb->getSpeed() << endl;

        processBeacons(wb);
    }
    else if (msg->getTreeId() == mHeartbeatEvt->getTreeId()) // heartbeat
    {
        DBG << "Heartbeat checking...\n";

        processHeartbeat();

        printNeighborList();

        mNode->scheduleAt(simTime() + (HEARTBEAT_THRESHOLD/10.0), mHeartbeatEvt);
    }
    else
    {
        switch (mState)
        {
            case IDLE:
            {
                EV << "IDLE: ";
                WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
                if (wbm == NULL) break;

                if (find(mRcvMessageId.begin(), mRcvMessageId.end(), wbm->getTreeId()) == mRcvMessageId.end())
                {
                    // New message
                    mNode->handleAccidentMsg();

                    addToBuffer(wbm->getTreeId());

                    double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);
                    mMinRelativeDistance = distance;
                    EV << "Original distance " << distance << endl;

                    mCurMsg = wbm->dup();
                    mCurMsg->setForwardAddress(mNode->myId);

                    if (mNeighborList.size() <= 2)
                    {
                        mNode->scheduleAt(simTime() + PACKET_EXPIRATION_TIME_IN_MS * 0.001, mPacketTimeoutEvt);
                        mState = STORE;
                        break;
                    }

                    EV << "Change to state WAIT after  "<< WAIT_TIME_IN_MS << "ms" << endl;
                    mNode->scheduleAt(simTime() + WAIT_TIME_IN_MS * 0.001, mTimeoutEvt);
                    mState = WAIT;
                }
                else
                {
                    // Duplicate
                }
                break;
            }

            case WAIT:
            {
                EV << "WAIT: ";
                if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
                {
                    int slotNum = calcSlotNumber();

                    EV << "Slot number = " << slotNum << endl;

                    double waitTime = slotNum * mNode->mPropagationDelay;
                    EV << "Change to state WAIT_SLOT after " << waitTime << " second\n";
                    mNode->scheduleAt(simTime() + waitTime, mTimeoutEvt);
                    mState = WAIT_SLOT;
                }
                else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
                {
                    WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);

                    double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);

                    if (mMinRelativeDistance > distance)
                        mMinRelativeDistance = distance;

                    EV << "Re-calculate the min distance: " << mMinRelativeDistance << endl ;
                }
            }
            break;

            case WAIT_SLOT:
            {
                EV << "WAIT_SLOT: ";
                if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
                {
                    EV << "Change to state SEND\n";
                    mState = SEND;
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                }
                else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
                {
                    mNode->cancelEvent(mTimeoutEvt);

                    EV << "Going back to IDLE after duplication\n";
                    mState = IDLE;
                }
                break;
            }

            case SEND:
                EV << "SEND: \n";
                if (msg->getTreeId() == mTimeoutEvt->getTreeId())
                {
                    mCurMsg->setForwardPos(mNode->curPosition);
                    mNode->sendWBM(mCurMsg->dup());

                    if (isSF(mCurMsg->getSenderPos()))
                    {
                        DBG << "I am storing the message\n";
                        if (mPacketTimeoutEvt->isScheduled() == false)
                            mNode->scheduleAt(simTime() + PACKET_EXPIRATION_TIME_IN_MS * 0.001, mPacketTimeoutEvt);
                        //TODO: what if mTimeoutEvt and mPacketTimeoutEvt happens at the same time.

                        mState = STORE;
                        mNode->findHost()->getDisplayString().updateWith("r=16,yellow");
                    }
                    else
                    {
                        mState = IDLE;
                        mNode->findHost()->getDisplayString().updateWith("r=16,green");
                    }

                    break;
                }
                break;

            case STORE:
                EV << "STORE\n";
                if (msg->getTreeId() == mTimeoutEvt->getTreeId())
                {
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                    mState = SEND;
                    break;
                }
                else if (msg->getTreeId() == mPacketTimeoutEvt->getTreeId())
                {
                    mState = IDLE;
                    break;
                }
                break;
        }
    }
}

void BIM::sendWarningMsg(const std::string& msg)
{
    WaveBroadcastMessage *wbm = mNode->prepareWBM("broadcast", mNode->dataLengthBits, type_CCH, mNode->dataPriority, -1, 2);
    wbm->setWsmData(msg.c_str());

    addToBuffer(wbm->getTreeId());
    mCurMsg = wbm->dup();
    mNode->sendWBM(wbm);
    mState = STORE;
}

void BIM::addToBuffer(long value)
{
    mRcvMessageId[mBufferIdx] = value;
    mBufferIdx = (mBufferIdx + 1) % BUFFER_SIZE;
}

void BIM::processBeacons(WaveBroadcastBeacon* wb)
{
    if (mNeighborList.find(wb->getSenderAddress()) == mNeighborList.end())
    {
        Neighbor* newNeighbor = new Neighbor(wb->getSenderPos(), wb->getSpeed(), wb->getDirection(), simTime());
        mNeighborList[wb->getSenderAddress()] = newNeighbor;
    }

    mNeighborList[wb->getSenderAddress()]->timeStamp = simTime();
    mNeighborList[wb->getSenderAddress()]->position = wb->getSenderPos();
    mNeighborList[wb->getSenderAddress()]->speed = wb->getSpeed();
    mNeighborList[wb->getSenderAddress()]->direction = wb->getDirection();

    if (mState == STORE && wb->getHasMessage() == 0)
    {
//        DBG << "New neighbor, rebroadcast\n";
        DBG << "Neighbor has not received packet, rebroadcast\n";
        if (mTimeoutEvt->isScheduled() == false)
        {
            mNode->scheduleAt(simTime() + WAIT_TIME_IN_MS * 0.001, mTimeoutEvt);
        }
    }
}

void BIM::processHeartbeat()
{
    map<int,Neighbor*>::iterator iter = mNeighborList.begin();
    simtime_t current = simTime();
    for (; iter != mNeighborList.end();)
    {
        if (iter->first != mNode->myId && current - iter->second->timeStamp > HEARTBEAT_THRESHOLD) //miss two beacons
        {
            DBG << "delete " << iter->first << endl;
            delete iter->second;
            mNeighborList.erase(iter++);
        }
        else
        {
            ++iter;
        }
    }
}

void BIM::printNeighborList()
{
    map<int,Neighbor*>::const_iterator iter = mNeighborList.begin();
    for (; iter != mNeighborList.end(); ++iter)
    {
        DBG << iter->first << " " << iter->second->timeStamp << " " << iter->second->position << " " << iter->second->speed << " " <<
                iter->second->direction << endl;
    }
}

int BIM::calcSlotNumber()
{
    int slotNum = (double)NUM_OF_SLOTS *
            (1 - min(mMinRelativeDistance, (double)NodeMain::TRANSMISSION_RANGE)/(double)NodeMain::TRANSMISSION_RANGE);

    return slotNum;
}

bool BIM::isSF(const Coord& aSender)
{
//    double rotateAngle = -mNode->curDirection;
    double rotateAngle = -mNeighborList[mNode->myId]->direction;
    const Coord& myPos = getPredictedPosition(mNeighborList[mNode->myId]);
    double myX = myPos.x * cos(rotateAngle) - myPos.y * sin(rotateAngle);
    double theirX = aSender.x * cos(rotateAngle) - aSender.y * sin(rotateAngle);

    printNeighborList();
    DBG << "Current direction = " << mNode->curDirection << endl;
    DBG << "myX = " << myX << "; theirX = " << theirX << endl;

    map<int,Neighbor*>::const_iterator iter = mNeighborList.begin();
    for (; iter != mNeighborList.end(); ++iter)
        if (iter->first != mNode->myId && fabs(iter->second->direction - mNeighborList[mNode->myId]->direction) < M_PI/6)
        {
            Neighbor* neighbor = iter->second;
            const Coord& neighborPos = getPredictedPosition(neighbor);
            double neighborX = neighborPos.x * cos(rotateAngle) - neighborPos.y * sin(rotateAngle);
            DBG << "ID = " << iter->first << "; neighborX = " << neighborX << endl;
            if ((neighborX < myX) && (myX < theirX)) return false;
            if ((neighborX > myX) && (myX > theirX)) return false;
        }
    return true;
}

double BIM::calcBeaconInterval()
{
    if (mNeighborList.size() <= 1)
        return 1;

    cStdDev neighborSpeed;

    map<int, Neighbor*>::const_iterator iter = mNeighborList.begin();
    for (; iter != mNeighborList.end(); ++iter)
    {
        neighborSpeed.collect(iter->second->speed);
    }

    double stdDev = neighborSpeed.getStddev();
    double mean = neighborSpeed.getMean();
    double cv = stdDev/mean;

    DBG << "Speed dev = " << stdDev << endl;
    DBG << "Mean = " << mean << endl;

    double beaconInterval = 1;
    if (cv < 1) beaconInterval = BEACON_MAX_INTERVAL - (BEACON_MAX_INTERVAL - 1) * cv;

    DBG << "Beacon interval = " << beaconInterval << endl;

    return beaconInterval;
}

double BIM::calcProbability()
{
    EV << "Distance = " << mMinRelativeDistance << endl;
    return (mMinRelativeDistance/(mNode->TRANSMISSION_RANGE * 1.0));
}

Coord BIM::getPredictedPosition(const Coord& position,
        double direction, double speed, const simtime_t& timeStamp)
{
    Coord pPos;

    double dist = (simTime() - timeStamp).dbl() * speed;

    pPos.x = position.x + dist * cos(direction);
    pPos.y = position.y + dist * sin(direction);

    return pPos;
}

Coord BIM::getPredictedPosition(Neighbor* aNeighbor)
{
    return getPredictedPosition(aNeighbor->position, aNeighbor->direction, aNeighbor->speed, aNeighbor->timeStamp);
}
