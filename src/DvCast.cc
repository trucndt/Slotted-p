/*
 * DvCast.cpp
 *
 *  Created on: Jun 17, 2017
 *      Author: trucndt
 */

#include "DvCast.h"

using namespace std;

DvCast::DvCast(NodeMain* aNodeMain)
{
    mNode = aNodeMain;
    mTimeoutEvt = new cMessage("Timeout");
    mRcvMessageId.resize(BUFFER_SIZE, -1);
    mBufferIdx = 0;
    mHeartbeatEvt = new cMessage("Heartbeat");
    mNode->scheduleAt(simTime() + (HEARTBEAT_THRESHOLD/10.0), mHeartbeatEvt);
    mPacketTimeoutEvt = new cMessage("PktTimeout");
    mState = IDLE;

    mODC = mMDC = 0;
}

void DvCast::processReceivedMessage(cMessage* msg)
{
    if (WaveBroadcastBeacon* wb = dynamic_cast<WaveBroadcastBeacon*>(msg))
    {
        // it is a beacon
        DBG << "Direction: " << wb->getDirection() << endl;
        DBG << "Speed: " << wb->getSpeed() << endl;

        processBeacons(wb);
    }
    else if (msg->getTreeId() == mHeartbeatEvt->getTreeId()) // heartbeat
    {
        DBG << "Heartbeat checking...\n";

        processHeartbeat(mNBback);
        processHeartbeat(mNBfront);

        processHeartbeat(mNBopposite);
        if (mNBopposite.empty()) mODC = 0;

        printNeighborLists();

        mNode->scheduleAt(simTime() + (HEARTBEAT_THRESHOLD/10.0), mHeartbeatEvt);
    }
    else
    {
        switch (mState)
        {
        case IDLE:
        {
            EV << "IDLE\n";
            WaveBroadcastMessage *wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
            if (wbm == NULL)
            {
                DBG << "ERR: Unknown message " << msg->getFullName() << endl;
                return;
            }

            if (find(mRcvMessageId.begin(), mRcvMessageId.end(), wbm->getTreeId()) == mRcvMessageId.end())
            {
                // New message
                mNode->handleAccidentMsg();

                addToBuffer(wbm->getTreeId());
                double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);
                mMinRelativeDistance = distance;
                EV << "Original distance " << distance << endl;

                mCurMsg = wbm->dup();
                mCurMsg->setForwardPos(mNode->curPosition);
                mCurMsg->setForwardAddress(mNode->myId);

                /* Neighbor detection */
                if ((fabs(mNode->curDirection - mCurMsg->getIntendedDirection()) < M_PI_2
                && !mNBfront.empty())
                || (fabs(mNode->curDirection - mCurMsg->getIntendedDirection()) >= M_PI_2
                && !mNBback.empty()))
                {
                    EV << "Well-connected. Change to state WAIT after " << WAIT_TIME_IN_MS << " ms" << endl;
                    mNode->scheduleAt(simTime() + WAIT_TIME_IN_MS * 0.001, mTimeoutEvt);
                    mState = WAIT;
                }
                else if (mODC == 1)
                {
                    EV << "Opposite neighbor. Change to state HOLD2 " << endl;
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                    mState = HOLD2;

                    EV << "Packet expires in " << PACKET_EXPIRATION_TIME_IN_MS << "ms\n";
                    mNode->scheduleAt(simTime() + PACKET_EXPIRATION_TIME_IN_MS * 0.001, mPacketTimeoutEvt);
                }
                else
                {
                    EV << "Opposite neighbor. Change to state HOLD1 " << endl;
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                    mState = HOLD1;

                    EV << "Packet expires in " << PACKET_EXPIRATION_TIME_IN_MS << "ms\n";
                    mNode->scheduleAt(simTime() + PACKET_EXPIRATION_TIME_IN_MS * 0.001, mPacketTimeoutEvt);
                }
            }
            else
            {
                // Duplicate
            }
            break;
        }

        case WAIT:
            EV << "WAIT: \n";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
            {
                double probability = calcProbability();

                if (mNode->dblrand() <= probability)
                {
                    EV << "Change to state SEND " << endl;
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                    mState = SEND;
                }
                else
                {
                    // base on the probability
                    double waitTime = (double)(mNode->intrand(1000 - 1000*probability)
                            + WAIT_TIME_IN_MS * 1000) * pow(10, -6) + mNode->mPropagationDelay;

                    EV << "Change to state BACKOFF after " << waitTime << " seconds" << endl;
                    mNode->scheduleAt(simTime() + waitTime, mTimeoutEvt);
                    mState = BACKOFF;
                }
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
            {
                WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);

                double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);

                if (mMinRelativeDistance > distance)
                    mMinRelativeDistance = distance;

                EV << "Re-calculate the probability: " << mMinRelativeDistance << endl ;
            }
            break;

        case BACKOFF:
            EV << "BACKOFF: \n";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
            {
                EV << "Change to state SEND " << endl;
                mNode->scheduleAt(simTime(), mTimeoutEvt);
                mState = SEND;
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
            {
                mNode->cancelEvent(mTimeoutEvt);

                EV << "Going back to IDLE after duplication\n";
                mState = IDLE;
            }
            break;

        case SEND:
            EV << "SEND: \n";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                mCurMsg->setForwardPos(mNode->curPosition);
                mNode->sendWBM(mCurMsg);
                mState = IDLE;
                break;
            }
            break;

        case HOLD2:
            EV << "HOLD2: \n";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                mCurMsg->setForwardPos(mNode->curPosition);
                mNode->sendWBM(mCurMsg->dup());
                break;
            }
            else if (msg->getTreeId() == mPacketTimeoutEvt->getTreeId())
            {
                mState = IDLE;
                break;
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId())
            {
                WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
                if (wbm->getHopCount() >= mCurMsg->getHopCount() + 2)
                {
                    mNode->cancelEvent(mPacketTimeoutEvt);
                    mState = IDLE;
                    break;
                }
            }
            break;

        case HOLD1:
            EV << "HOLD1: \n";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                if ((fabs(mNode->curDirection - mCurMsg->getIntendedDirection()) < M_PI_2
                && !mNBfront.empty())
                || (fabs(mNode->curDirection - mCurMsg->getIntendedDirection()) >= M_PI_2
                && !mNBback.empty()) || !mNBopposite.empty())
                {
                    mCurMsg->setForwardPos(mNode->curPosition);
                    mNode->sendWBM(mCurMsg->dup());
                    mNode->cancelEvent(mPacketTimeoutEvt);
                    mState = IDLE;
                }
                break;
            }
            else if (msg->getTreeId() == mPacketTimeoutEvt->getTreeId())
            {
                mState = IDLE;
                break;
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId())
            {
                WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
                if (wbm->getHopCount() >= mCurMsg->getHopCount() + 2)
                {
                    mNode->cancelEvent(mPacketTimeoutEvt);
                    mState = IDLE;
                    break;
                }
            }
            break;
        }
    }
}

void DvCast::sendWarningMsg(const std::string& msg)
{
    WaveBroadcastMessage *wbm = mNode->prepareWBM("broadcast", mNode->dataLengthBits, type_CCH, mNode->dataPriority, -1, 2);
    wbm->setWsmData(msg.c_str());
    wbm->setIntendedDirection(mNode->curDirection + M_PI);
    addToBuffer(wbm->getTreeId());
    mCurMsg = wbm->dup();
    mNode->sendWBM(wbm);
    mState = HOLD1;
}

void DvCast::printList(const std::map<int,simtime_t>& list)
{
    map<int,simtime_t>::const_iterator iter = list.begin();
    for (; iter != list.end(); ++iter)
    {
        DBG << iter->first << " " << iter->second << endl;
    }
}

void DvCast::printNeighborLists()
{
    DBG << "Front neighbors: " << endl;
    printList(mNBfront);
    DBG << "Back neighbors: " << endl;
    printList(mNBback);
    DBG << "Opposite neighbors: " << endl;
    printList(mNBopposite);
}

void DvCast::processBeacons(WaveBroadcastBeacon* wb)
{
    double theirDirection = wb->getDirection();
    if (fabs(theirDirection - mNode->curDirection) < M_PI_2)
    {
        DBG << "Same direction" << endl;

        /* Apply rotation of coordinate system */
        double rotateAngle;
        if (mNode->curDirection < 0)
        {
            rotateAngle = -(2 * M_PI + mNode->curDirection);
        }
        else rotateAngle = -mNode->curDirection;

        double theirX = wb->getSenderPos().x * cos(rotateAngle) - wb->getSenderPos().y * sin(rotateAngle);
        double myX = mNode->curPosition.x * cos(rotateAngle) - mNode->curPosition.y * sin(rotateAngle);

        if (myX < theirX)
        {
            mNBfront[wb->getSenderAddress()] = simTime();
        }
        else
        {
            mNBback[wb->getSenderAddress()] = simTime();
            mMDC = 1;
        }

        if (mState == HOLD1)
        {
            DBG << "Rebroadcast\n";
            mNode->scheduleAt(simTime(), mTimeoutEvt);
        }
    }
    else
    {
        DBG << "Opposite direction" << endl;
        if (mNBopposite.find(wb->getSenderAddress()) == mNBopposite.end())
        {
            DBG << "New opposite neighbor" << endl;
            if (mState == HOLD2 || mState == HOLD1)
            {
                DBG << "Rebroadcast\n";
                mNode->scheduleAt(simTime(), mTimeoutEvt);
            }
        }
        mNBopposite[wb->getSenderAddress()] = simTime();
        mODC = 1;
    }

    printNeighborLists();
}

void DvCast::processMessages(WaveBroadcastMessage* wbm)
{

}

void DvCast::processHeartbeat(std::map<int,simtime_t>& list)
{
    map<int,simtime_t>::iterator iter = list.begin();
    simtime_t current = simTime();
    for (; iter != list.end();)
    {
        if (current - iter->second > HEARTBEAT_THRESHOLD) //miss two beacons
        {
            DBG << "delete " << iter->first << endl;
            list.erase(iter++);
        }
        else
        {
            ++iter;
        }
    }
}

double DvCast::calcProbability()
{
    EV << "Distance = " << mMinRelativeDistance << endl;
    return (mMinRelativeDistance/(mNode->TRANSMISSION_RANGE * 1.0));
}

void DvCast::addToBuffer(long value)
{
    mRcvMessageId[mBufferIdx] = value;
    mBufferIdx = (mBufferIdx + 1) % BUFFER_SIZE;
}
