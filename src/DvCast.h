/*
 * DvCast.h
 *
 *  Created on: Jun 17, 2017
 *      Author: trucndt
 */

#ifndef DVCAST_H_
#define DVCAST_H_

#include <DataDissemination.h>
#include "NodeMain.h"
#include <stdint.h>
#include <vector>
#include <map>
#include <algorithm>

class NodeMain;

class DvCast: public DataDissemination
{
public:
    DvCast(NodeMain* aNodeMain);

    enum stateEnum
    {
        IDLE,
        WAIT,
        BACKOFF,
        SEND,
        HOLD1,
        HOLD2
    };

    void processReceivedMessage(cMessage *msg);
    void sendWarningMsg(const std::string &msg);

private:
    static const int BUFFER_SIZE = 5;
    static const int NB_BUFFER_SIZE = 10;
    static const int HEARTBEAT_THRESHOLD = 2;
    static const int WAIT_TIME_IN_MS = 6;
    static const int PACKET_EXPIRATION_TIME_IN_MS = 120000;

    NodeMain* mNode;
    cMessage* mTimeoutEvt;
    cMessage* mHeartbeatEvt;
    cMessage* mPacketTimeoutEvt;

    int mODC, mMDC;

    uint8_t mState;

    std::vector<long> mRcvMessageId;
    uint8_t mBufferIdx;
    std::map<int,simtime_t> mNBfront;
    std::map<int,simtime_t> mNBback;
    std::map<int,simtime_t> mNBopposite;

    WaveBroadcastMessage* mCurMsg;
    double mMinRelativeDistance;

    double calcProbability();
    void addToBuffer(long value);

    void printNeighborLists();
    void printList(const std::map<int,simtime_t>& list);

    void processBeacons(WaveBroadcastBeacon *wb);
    void processMessages(WaveBroadcastMessage *wbm);
    void processHeartbeat(std::map<int,simtime_t>& list);
};

#endif /* DVCAST_H_ */
