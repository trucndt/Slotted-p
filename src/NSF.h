/*
 * NSF.h
 *
 *  Created on: Jun 28, 2017
 *      Author: trucndt
 */

#ifndef NSF_H_
#define NSF_H_

#include <DataDissemination.h>
#include "NodeMain.h"
#include <stdint.h>
#include <vector>
#include <map>
#include <algorithm>

class NodeMain;

class NSF : public DataDissemination
{
public:
    NSF(NodeMain* aNodeMain);
    virtual ~NSF();

    enum stateEnum
    {
        IDLE,
        SEND,
        STORE
    };

    void processReceivedMessage(cMessage *msg);
    void sendWarningMsg(const std::string &msg);

private:
    static const int BUFFER_SIZE = 5;
    static const int HEARTBEAT_THRESHOLD = 2;
    static const int PACKET_EXPIRATION_TIME_IN_MS = 120000;
    static const int WAIT_TIME_IN_MS = 6;

    NodeMain* mNode;
    cMessage* mHeartbeatEvt;
    cMessage* mTimeoutEvt;
    cMessage* mPacketTimeoutEvt;

    uint8_t mState;

    std::vector<long> mRcvMessageId;
    uint8_t mBufferIdx;

    bool mWaitForBeacons;

    WaveBroadcastMessage* mCurMsg;

    std::map<int,simtime_t> mNeighborList;

    void addToBuffer(long value);
    void processBeacons(WaveBroadcastBeacon *wb);
    void processHeartbeat();
    void printNeighborList();
};

#endif /* NSF_H_ */
