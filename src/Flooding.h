/*
 * Flooding.h
 *
 *  Created on: Apr 22, 2017
 *      Author: trucndt
 */

#ifndef FLOODING_H_
#define FLOODING_H_

#include <stdint.h>
#include <vector>
#include <algorithm>
#include "DataDissemination.h"
#include "NodeMain.h"

class NodeMain;

class Flooding: public DataDissemination
{
public:
    Flooding(NodeMain* aNodeMain);

    enum stateEnum
    {
        IDLE,
        SEND
    };

    void processReceivedMessage(cMessage *msg);
    void sendWarningMsg(const std::string &msg);

private:
    static const int BUFFER_SIZE = 5;
    static const int MAX_FORWARD_DISTANCE = 2000;

    uint8_t mState;
    NodeMain* mNode;
    cMessage* mTimeoutEvt;

    std::vector<long> mRcvMessageId;
    uint8_t mBufferIdx;

    WaveBroadcastMessage* mCurMsg;
    double mMinProbability;

    void addToBuffer(long value);
};

#endif /* FLOODING_H_ */
