/*
 * Flooding.cpp
 *
 *  Created on: Apr 22, 2017
 *      Author: trucndt
 */

#include <Flooding.h>

#include <WeightedP.h>

using namespace std;

Flooding::Flooding(NodeMain* aNodeMain)
{
    mNode = aNodeMain;
    mState = IDLE;
    mTimeoutEvt = new cMessage("Timeout");
    mRcvMessageId.resize(BUFFER_SIZE);
    mBufferIdx = 0;
}

void Flooding::processReceivedMessage(cMessage* msg)
{
    //TODO: Receive a new packet during WAIT, BACKOFF, SEND. At each state, if a new packet arrived, put it in a queue.

    switch (mState)
    {
        case IDLE:
        {
            EV << "IDLE: ";
            WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
            if (wbm == NULL) break;

            if (find(mRcvMessageId.begin(), mRcvMessageId.end(), wbm->getTreeId()) == mRcvMessageId.end())
            {
                // New message
                // Discard if not in ROI
                mNode->handleAccidentMsg();
                if (mNode->traci->getDistance(mNode->curPosition, wbm->getSenderPos(), false) > MAX_FORWARD_DISTANCE)
                {
//                    break;
                }

                addToBuffer(wbm->getTreeId());

                mCurMsg = wbm->dup();
                mCurMsg->setForwardPos(mNode->curPosition);
                mCurMsg->setForwardAddress(mNode->myId);

                EV << "Change to state SEND" << endl;
                mNode->scheduleAt(simTime(), mTimeoutEvt);
                mState = SEND;
            }
            else
            {
                // Duplicate
            }
            break;
        }
        case SEND:
            EV << "SEND: ";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                mNode->sendWBM(mCurMsg);
                mState = IDLE;
                break;
            }
    }
}

void Flooding::sendWarningMsg(const string& msg)
{
    WaveBroadcastMessage *wbm = mNode->prepareWBM("broadcast", mNode->dataLengthBits, type_CCH, mNode->dataPriority, -1, 2);
    wbm->setWsmData(msg.c_str());
    addToBuffer(wbm->getTreeId());
    mCurMsg = wbm->dup();
    mNode->sendWBM(wbm);
}

void Flooding::addToBuffer(long value)
{
    mRcvMessageId[mBufferIdx] = value;
    mBufferIdx = (mBufferIdx + 1) % BUFFER_SIZE;
}
