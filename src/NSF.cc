/*
 * NSF.cc
 *
 *  Created on: Jun 28, 2017
 *      Author: trucndt
 */

#include <NSF.h>

using namespace std;

NSF::NSF(NodeMain* aNodeMain)
{
    mNode = aNodeMain;
    mRcvMessageId.resize(BUFFER_SIZE, -1);
    mBufferIdx = 0;
    mHeartbeatEvt = new cMessage("Heartbeat");
    mTimeoutEvt = new cMessage("Timeout");
    mNode->scheduleAt(simTime() + (HEARTBEAT_THRESHOLD/10.0), mHeartbeatEvt);
    mPacketTimeoutEvt = new cMessage("PktTimeout");
    mState = IDLE;
    mWaitForBeacons = false;
}

NSF::~NSF()
{
    // TODO Auto-generated destructor stub
}

void NSF::processReceivedMessage(cMessage* msg)
{
    if (WaveBroadcastBeacon* wb = dynamic_cast<WaveBroadcastBeacon*>(msg))
    {
        // it is a beacon
        DBG << "Direction: " << wb->getDirection() << endl;
        DBG << "Speed: " << wb->getSpeed() << endl;

        processBeacons(wb);
    }
    else if (msg->getTreeId() == mHeartbeatEvt->getTreeId()) // heartbeat
    {
        DBG << "Heartbeat checking...\n";

        processHeartbeat();

        printNeighborList();

        mNode->scheduleAt(simTime() + (HEARTBEAT_THRESHOLD/10.0), mHeartbeatEvt);
    }
    else
    {
        switch (mState)
        {
        case IDLE:
        {
            EV << "IDLE\n";
            WaveBroadcastMessage *wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
            if (wbm == NULL)
            {
                DBG << "ERR: Unknown message " << msg->getFullName() << endl;
                return;
            }
            if (find(mRcvMessageId.begin(), mRcvMessageId.end(), wbm->getTreeId()) == mRcvMessageId.end())
            {
                // New message
                mNode->handleAccidentMsg();

                addToBuffer(wbm->getTreeId());

                mCurMsg = wbm->dup();
                mCurMsg->setForwardPos(mNode->curPosition);
                mCurMsg->setForwardAddress(mNode->myId);

                mNode->scheduleAt(simTime() + PACKET_EXPIRATION_TIME_IN_MS * 0.001, mPacketTimeoutEvt);

                if (mNeighborList.size() > 1)
                {
                    mState = SEND;
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                }
                else
                {
                    mState = STORE;
                }
            }
            else
            {
                // Duplicate
            }
            break;
        }

        case SEND:
            EV << "SEND: \n";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                mCurMsg->setForwardPos(mNode->curPosition);
                mNode->sendWBM(mCurMsg->dup());
                mState = STORE;
                break;
            }
            break;

        case STORE:
            EV << "STORE: \n";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                mNode->scheduleAt(simTime(), mTimeoutEvt);
                mState = SEND;
                mWaitForBeacons = false;
                break;
            }
            else if (msg->getTreeId() == mPacketTimeoutEvt->getTreeId())
            {
                mState = IDLE;
                break;
            }
            break;
        default:
            break;
        }
    }
}

void NSF::sendWarningMsg(const std::string& msg)
{
    WaveBroadcastMessage *wbm = mNode->prepareWBM("broadcast", mNode->dataLengthBits, type_CCH, mNode->dataPriority, -1, 2);
    wbm->setWsmData(msg.c_str());

    addToBuffer(wbm->getTreeId());
    mCurMsg = wbm->dup();
    mNode->sendWBM(wbm);
    mState = STORE;
}

void NSF::addToBuffer(long value)
{
    mRcvMessageId[mBufferIdx] = value;
    mBufferIdx = (mBufferIdx + 1) % BUFFER_SIZE;
}

void NSF::processBeacons(WaveBroadcastBeacon* wb)
{
    if (mNeighborList.find(wb->getSenderAddress()) == mNeighborList.end())
    {
        if (mState == STORE)
        {
            DBG << "New neighbor, rebroadcast\n";
            if (!mWaitForBeacons)
            {
                mNode->scheduleAt(simTime() + WAIT_TIME_IN_MS * 0.001, mTimeoutEvt);
                mWaitForBeacons = true;
            }
        }
    }

    mNeighborList[wb->getSenderAddress()] = simTime();
}

void NSF::processHeartbeat()
{
    map<int,simtime_t>::iterator iter = mNeighborList.begin();
    simtime_t current = simTime();
    for (; iter != mNeighborList.end();)
    {
        if (current - iter->second > HEARTBEAT_THRESHOLD) //miss two beacons
        {
            DBG << "delete " << iter->first << endl;
            mNeighborList.erase(iter++);
        }
        else
        {
            ++iter;
        }
    }
}

void NSF::printNeighborList()
{
    map<int,simtime_t>::const_iterator iter = mNeighborList.begin();
    for (; iter != mNeighborList.end(); ++iter)
    {
        DBG << iter->first << " " << iter->second << endl;
    }
}
