/*
 * WeightedPPersistence.cpp
 *
 *  Created on: Apr 8, 2017
 *      Author: trucndt
 */

#include <WeightedP.h>

using namespace std;

WeightedP::WeightedP(NodeMain* aNodeMain)
{
    mNode = aNodeMain;
    mState = IDLE;
    mTimeoutEvt = new cMessage("Timeout");
    mRcvMessageId.resize(BUFFER_SIZE);
    mBufferIdx = 0;
}

void WeightedP::processReceivedMessage(cMessage* msg)
{
    //TODO: Receive a new packet during WAIT, BACKOFF, SEND. At each state, if a new packet arrived, put it in a queue.

    switch (mState)
    {
        case IDLE:
        {
            EV << "IDLE: ";
            WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);
            if (wbm == NULL) break;

            if (find(mRcvMessageId.begin(), mRcvMessageId.end(), wbm->getTreeId()) == mRcvMessageId.end())
            {
                // New message
                // Discard if not in ROI
                mNode->handleAccidentMsg();
                if (mNode->traci->getDistance(mNode->curPosition, wbm->getSenderPos(), false) > MAX_FORWARD_DISTANCE)
                {
//                    break;
                }

                addToBuffer(wbm->getTreeId());
                double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);
                mMinRelativeDistance = distance;
                EV << "Original distance " << distance << endl;

                mCurMsg = wbm->dup();
                mCurMsg->setForwardPos(mNode->curPosition);
                mCurMsg->setForwardAddress(mNode->myId);

                EV << "Change to state WAIT after " << WAIT_TIME_IN_MS << " ms" << endl;
                mNode->scheduleAt(simTime() + WAIT_TIME_IN_MS * 0.001, mTimeoutEvt);
                mState = WAIT;
            }
            else
            {
                // Duplicate
            }
            break;
        }
        case WAIT:
            EV << "WAIT: ";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
            {
                double probability = calcProbability();

                if (mNode->dblrand() <= probability)
                {
                    EV << "Change to state SEND " << endl;
                    mNode->scheduleAt(simTime(), mTimeoutEvt);
                    mState = SEND;
                }
                else
                {
                    // base on the probability
                    double waitTime = (double)(mNode->intrand(1000 - 1000*probability)
                            + WAIT_TIME_IN_MS * 1000) * pow(10, -6) + mNode->mPropagationDelay;

                    EV << "Change to state BACKOFF after " << waitTime << " seconds" << endl;
                    mNode->scheduleAt(simTime() + waitTime, mTimeoutEvt);
                    mState = BACKOFF;
                }
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
            {
                WaveBroadcastMessage* wbm = dynamic_cast<WaveBroadcastMessage*>(msg);

                double distance = mNode->traci->getDistance(mNode->curPosition, wbm->getForwardPos(), false);

                if (mMinRelativeDistance > distance)
                    mMinRelativeDistance = distance;

                EV << "Re-calculate the probability: " << mMinRelativeDistance << endl ;
            }
            break;

        case BACKOFF:
            EV << "BACKOFF: ";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId()) //timeout
            {
                EV << "Change to state SEND " << endl;
                mNode->scheduleAt(simTime(), mTimeoutEvt);
                mState = SEND;
            }
            else if (msg->getTreeId() == mCurMsg->getTreeId()) //duplicate
            {
                mNode->cancelEvent(mTimeoutEvt);

                EV << "Going back to IDLE after duplication\n";
                mState = IDLE;
            }
            break;

        case SEND:
            EV << "SEND: ";
            if (msg->getTreeId() == mTimeoutEvt->getTreeId())
            {
                mNode->sendWBM(mCurMsg);
                mState = IDLE;
                break;
            }
    }
}

void WeightedP::sendWarningMsg(const string& msg)
{
    WaveBroadcastMessage *wbm = mNode->prepareWBM("broadcast", mNode->dataLengthBits, type_CCH, mNode->dataPriority, -1, 2);
    wbm->setWsmData(msg.c_str());
    addToBuffer(wbm->getTreeId());
    mCurMsg = wbm->dup();
    mNode->sendWBM(wbm);
}

double WeightedP::calcProbability()
{
    EV << "Distance = " << mMinRelativeDistance << endl;
    return (mMinRelativeDistance/(mNode->TRANSMISSION_RANGE * 1.0));
}

void WeightedP::addToBuffer(long value)
{
    mRcvMessageId[mBufferIdx] = value;
    mBufferIdx = (mBufferIdx + 1) % BUFFER_SIZE;
}
