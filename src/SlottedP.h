/*
 * Slotted1.h
 *
 *  Created on: Apr 28, 2017
 *      Author: trucndt
 */

#ifndef SLOTTEDP_H_
#define SLOTTEDP_H_

#include <stdint.h>
#include <vector>
#include <algorithm>
#include "DataDissemination.h"
#include "NodeMain.h"

class NodeMain;

class SlottedP: public DataDissemination
{
public:
    SlottedP(NodeMain* aNodeMain, double aProbability = 1);

    enum stateEnum
    {
        IDLE,
        WAIT,
        WAIT_SLOT,
        BACKOFF,
        SEND
    };

    void processReceivedMessage(cMessage *msg);
    void sendWarningMsg(const std::string &msg);

private:
    static const int BUFFER_SIZE = 5;
    static const int MAX_FORWARD_DISTANCE = 2000;
    static const int NUM_OF_SLOTS = 4;
    static const int WAIT_TIME_IN_MS = 6;

    double mPredefinedProbability;

    uint8_t mState;
    NodeMain* mNode;
    cMessage* mTimeoutEvt;

    std::vector<long> mRcvMessageId;
    uint8_t mBufferIdx;

    WaveBroadcastMessage* mCurMsg;
    double mMinRelativeDistance;

    int calcSlotNumber();
    void addToBuffer(long value);
};

#endif /* SLOTTEDP_H_ */
