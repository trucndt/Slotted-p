/*
 * BIM.h
 *
 *  Created on: Jun 28, 2017
 *      Author: trucndt
 */

#ifndef BIM_H_
#define BIM_H_

#include <DataDissemination.h>
#include "NodeMain.h"
#include <stdint.h>
#include <vector>
#include <map>
#include <algorithm>

class NodeMain;
class Neighbor;

class BIM : public DataDissemination
{
public:
    BIM(NodeMain* aNodeMain);
    virtual ~BIM();

    enum stateEnum
    {
        IDLE,
        WAIT,
        BACKOFF,
        WAIT_SLOT,
        SEND,
        STORE
    };

    void processReceivedMessage(cMessage *msg);
    void sendWarningMsg(const std::string &msg);
    double calcBeaconInterval();

    WaveBroadcastMessage* mCurMsg;

private:
    static const int BUFFER_SIZE = 5;
    static const int HEARTBEAT_THRESHOLD = 5;
    static const int PACKET_EXPIRATION_TIME_IN_MS = 120000;
    static const int WAIT_TIME_IN_MS = 6;
    static const int NUM_OF_SLOTS = 4;
    static constexpr double BEACON_MAX_INTERVAL = 3;

    NodeMain* mNode;
    cMessage* mHeartbeatEvt;
    cMessage* mTimeoutEvt;
    cMessage* mPacketTimeoutEvt;

    uint8_t mState;

    std::vector<long> mRcvMessageId;
    uint8_t mBufferIdx;
    double mMinRelativeDistance;

    std::map<int,Neighbor*> mNeighborList;
    Coord getPredictedPosition(const Coord& position, double direction, double speed, const simtime_t& timeStamp);
    Coord getPredictedPosition(Neighbor* aNeighbor);

    void addToBuffer(long value);
    void processBeacons(WaveBroadcastBeacon *wb);
    void processHeartbeat();
    void printNeighborList();
    int calcSlotNumber();
    bool isSF(const Coord& aSender);
    double calcProbability();
};

#endif /* BIM_H_ */
